// initiated by Benoît Launay during an paged.js print on June

class forensic extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    beforeParsed(content) {
        let pannel = document.createElement("aside");

        let the_list = document.createElement("ul");
        pannel.style = "width:max-content;position : fixed;top:1rem;left:1rem;border:1px solid purple;padding:0 1em;z-index:999999;background-color:rgba(255,255,255,.8);max-height:45vh;overflow: auto;";
        pannel.insertAdjacentHTML(`beforeend`,
            `<h4>Classes spotted in source doc</h4>`
        );
        pannel.appendChild(the_list);
        // let’s get all the classes affected to elements
        const sections = content.querySelectorAll('*');
        var classes = [];
        sections.forEach(element => {
            if (element.classList.value != '') { // if element has some class
                if (element.classList.value.includes(" ")) { // more than one class?
                    let coupage = element.classList.value.split(" ");
                    coupage.forEach(a_class => {
                        classes.push(a_class);
                    });
                } else { // one class?
                    classes.push(element.classList.value);
                }
            }
        });
        classes.sort(); // class name sorting
        let uniqueClassStack = new Set(classes); // getting unique values
        uniqueClassStack.forEach(the_classes => {
            the_list.insertAdjacentHTML(`beforeend`,
                `<li><p style="margin-bottom:0">${the_classes}</p></li>`
            );
        });
        the_list.childNodes.forEach(item => { // highlighting concerned elements
            item.querySelector("p").addEventListener("click", (e) => {
                let info_plus = document.createElement("div");
                let selecteur = "." + item.querySelector("p").textContent;
                let selected = document.querySelectorAll(selecteur);
                if (item.querySelector("p").style.backgroundColor === "pink") {
                    item.querySelector("p").style.backgroundColor = "transparent";
                    selected.forEach(lm => {
                        lm.style.backgroundColor = "transparent"
                    });
                    item.removeChild(item.querySelector("div"));
                } else {
                    var the_classes = [];
                    var the_elements = [];
                    item.querySelector("p").style.backgroundColor = "pink";
                    selected.forEach(elem => {
                        elem.style.backgroundColor = "pink";
                        if (elem.classList != item.textContent) {
                            the_classes.push(elem.classList.value);// getting associated classes
                        }
                        the_elements.push(elem.localName);// getting associated elements
                    });
                    the_classes.sort();
                    the_elements.sort();
                    the_classes = new Set(the_classes);
                    the_elements = new Set(the_elements);
                    if (the_classes.size !== 0) {
                        console.log(the_classes);
                        info_plus.insertAdjacentHTML(`beforeend`,
                            `<p style="font-weight:bold;margin-bottom:0;margin-top:.33em;">linked classe(s) stack(s):</p>`
                        );
                        the_classes.forEach(the_class => {
                            info_plus.insertAdjacentHTML(`beforeend`,
                                `<p style="margin:0">❱ ${the_class}</p>`
                            );
                        });
                    }
                    info_plus.insertAdjacentHTML(`beforeend`,
                        `<p style="font-weight:bold;margin-bottom:0;margin-top:.33em;">concerned element(s):</p>`
                    );
                    the_elements.forEach(the_element => {
                        info_plus.insertAdjacentHTML(`beforeend`,
                            `<p style="margin:0">❱ ${the_element}</p>`
                        );
                    });
                    item.appendChild(info_plus);
                }
            });
        });
        document.querySelector("body").appendChild(pannel);
    }
}
Paged.registerHandlers(forensic);